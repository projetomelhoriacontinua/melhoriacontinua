terraform {
  backend "azurerm" {
    resource_group_name  = "RG-STG"
    storage_account_name = "pmcstgtf"
    container_name       = "tfstate"
    key                  = "PMCH.terraform.tfstate"
  }
  
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.91.0"
      }
    }
}
# Configure the Microsoft Azure Provider
  provider "azurerm" {
  features {}
}
